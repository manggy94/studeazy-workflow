<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersMatterRepository")
 */
class UsersMatter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="usersMatters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matter", inversedBy="usersMatters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Matter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getMatter(): ?Matter
    {
        return $this->Matter;
    }

    public function setMatter(?Matter $Matter): self
    {
        $this->Matter = $Matter;

        return $this;
    }
}
