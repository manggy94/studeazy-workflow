<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LessonRepository")
 */
class Lesson
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
<<<<<<< HEAD
     * @ORM\Column(type="integer")
     */
    private $TeacherId;

    /**
     * @ORM\Column(type="integer")
     */
    private $ScholarId;

    /**
     * @ORM\Column(type="integer")
     */
    private $EventId;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $Start;

    /**
     * @ORM\Column(type="time")
     */
    private $End;

    /**
     * @ORM\Column(type="dateinterval")
     */
    private $Duration;
=======
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="lessons")
     */
    private $Teacher;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $Student;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TimeSlot", cascade={"persist", "remove"})
     */
    private $TimeSlot;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matter", inversedBy="lessons")
     */
    private $Matter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $IsAccepted;
>>>>>>> agenda

    /**
     * @ORM\Column(type="boolean")
     */
<<<<<<< HEAD
    private $IsPast;

    /**
     * @ORM\Column(type="integer")
     */
    private $MatterId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsValidated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsDone;

    /**
     * @ORM\Column(type="float")
=======
    private $IsPaid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $IsDone;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $HourlyPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
>>>>>>> agenda
     */
    private $Price;

    public function getId(): ?int
    {
        return $this->id;
    }

<<<<<<< HEAD
    public function getTeacherId(): ?int
    {
        return $this->TeacherId;
    }

    public function setTeacherId(int $TeacherId): self
    {
        $this->TeacherId = $TeacherId;
=======
    public function getTeacher(): ?User
    {
        return $this->Teacher;
    }

    public function setTeacher(?User $Teacher): self
    {
        $this->Teacher = $Teacher;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getScholqrId(): ?int
    {
        return $this->ScholqrId;
    }

    public function setScholqrId(int $ScholqrId): self
    {
        $this->ScholqrId = $ScholqrId;
=======
    public function getStudent(): ?User
    {
        return $this->Student;
    }

    public function setStudent(?User $Student): self
    {
        $this->Student = $Student;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getEventId(): ?int
    {
        return $this->EventId;
    }

    public function setEventId(int $EventId): self
    {
        $this->EventId = $EventId;
=======
    public function getTimeSlot(): ?TimeSlot
    {
        return $this->TimeSlot;
    }

    public function setTimeSlot(?TimeSlot $TimeSlot): self
    {
        $this->TimeSlot = $TimeSlot;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;
=======
    public function getMatter(): ?Matter
    {
        return $this->Matter;
    }

    public function setMatter(?Matter $Matter): self
    {
        $this->Matter = $Matter;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getStart(): ?\DateTimeInterface
    {
        return $this->Start;
    }

    public function setStart(\DateTimeInterface $Start): self
    {
        $this->Start = $Start;
=======
    public function getIsAccepted(): ?bool
    {
        return $this->IsAccepted;
    }

    public function setIsAccepted(?bool $IsAccepted): self
    {
        $this->IsAccepted = $IsAccepted;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getEnd(): ?\DateTimeInterface
    {
        return $this->End;
    }

    public function setEnd(\DateTimeInterface $End): self
    {
        $this->End = $End;
=======
    public function getIsPaid(): ?bool
    {
        return $this->IsPaid;
    }

    public function setIsPaid(bool $IsPaid): self
    {
        $this->IsPaid = $IsPaid;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getDuration(): ?\DateInterval
    {
        return $this->Duration;
    }

    public function setDuration(\DateInterval $Duration): self
    {
        $this->Duration = $Duration;

        return $this;
    }

    public function getIsPast(): ?bool
    {
        return $this->IsPast;
    }

    public function setIsPast(bool $IsPast): self
    {
        $this->IsPast = $IsPast;

        return $this;
    }

    public function getMatterId(): ?int
    {
        return $this->MatterId;
    }

    public function setMatterId(int $MatterId): self
    {
        $this->MatterId = $MatterId;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->IsValidated;
    }

    public function setIsValidated(bool $IsValidated): self
    {
        $this->IsValidated = $IsValidated;
=======
    public function getIsDone(): ?bool
    {
        return $this->IsDone;
    }

    public function setIsDone(?bool $IsDone): self
    {
        $this->IsDone = $IsDone;
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getIsDone(): ?bool
    {
        return $this->IsDone;
    }

    public function setIsDone(bool $IsDone): self
    {
        $this->IsDone = $IsDone;
=======
    public function getHourlyPrice(): ?float
    {
        return $this->HourlyPrice;
    }

    public function setHourlyPrice(?float $HourlyPrice): self
    {
        $this->HourlyPrice = $HourlyPrice;
>>>>>>> agenda

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->Price;
    }

    public function setPrice(float $Price): self
    {
        $this->Price = $Price;

        return $this;
    }
}
