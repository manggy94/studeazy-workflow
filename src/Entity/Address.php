<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Label;

    /**
     * @ORM\Column(type="float")
     */
    private $Lat;

    /**
     * @ORM\Column(type="float")
     */
    private $Lng;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $GmapId;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="address", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $StreetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Route;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Locality;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AdministrativeAreaLevel2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AdministrativeAreaLevel1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PostalCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->Label;
    }

    public function setLabel(string $Label): self
    {
        $this->Label = $Label;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->Lat;
    }

    public function setLat(float $Lat): self
    {
        $this->Lat = $Lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->Lng;
    }

    public function setLng(float $Lng): self
    {
        $this->Lng = $Lng;

        return $this;
    }

    public function getGmapId(): ?string
    {
        return $this->GmapId;
    }

    public function setGmapId(string $GmapId): self
    {
        $this->GmapId = $GmapId;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->StreetNumber;
    }

    public function setStreetNumber(?string $StreetNumber): self
    {
        $this->StreetNumber = $StreetNumber;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->Route;
    }

    public function setRoute(?string $Route): self
    {
        $this->Route = $Route;

        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->Locality;
    }

    public function setLocality(string $Locality): self
    {
        $this->Locality = $Locality;

        return $this;
    }

    public function getAdministrativeAreaLevel2(): ?string
    {
        return $this->AdministrativeAreaLevel2;
    }

    public function setAdministrativeAreaLevel2(?string $AdministrativeAreaLevel2): self
    {
        $this->AdministrativeAreaLevel2 = $AdministrativeAreaLevel2;

        return $this;
    }

    public function getAdministrativeAreaLevel1(): ?string
    {
        return $this->AdministrativeAreaLevel1;
    }

    public function setAdministrativeAreaLevel1(?string $AdministrativeAreaLevel1): self
    {
        $this->AdministrativeAreaLevel1 = $AdministrativeAreaLevel1;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->Country;
    }

    public function setCountry(string $Country): self
    {
        $this->Country = $Country;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->PostalCode;
    }

    public function setPostalCode(?string $PostalCode): self
    {
        $this->PostalCode = $PostalCode;

        return $this;
    }
}
