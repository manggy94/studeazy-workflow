<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvailabilityRepository")
 */
class Availability
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="availabilities")
     */
    private $User;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TimeSlot", cascade={"persist", "remove"})
     */
    private $TimeSlot;

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getTimeSlot(): ?TimeSlot
    {
        return $this->TimeSlot;
    }

    public function setTimeSlot(?TimeSlot $TimeSlot): self
    {
        $this->TimeSlot = $TimeSlot;

        return $this;
    }

  
}
