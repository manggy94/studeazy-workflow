<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DisponibilityRepository")
 */
class Disponibility
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $UserId;

    /**
     * @ORM\Column(type="integer")
     */
    private $EventId;

    /**
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @ORM\Column(type="time")
     */
    private $Start;

    /**
     * @ORM\Column(type="time")
     */
    private $End;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsPast;

    /**
     * @ORM\Column(type="dateinterval")
     */
    private $Duration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->UserId;
    }

    public function setUserId(int $UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    public function getEventId(): ?int
    {
        return $this->EventId;
    }

    public function setEventId(int $EventId): self
    {
        $this->EventId = $EventId;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->Start;
    }

    public function setStart(\DateTimeInterface $Start): self
    {
        $this->Start = $Start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->End;
    }

    public function setEnd(\DateTimeInterface $End): self
    {
        $this->End = $End;

        return $this;
    }

    public function getIsPast(): ?bool
    {
        return $this->IsPast;
    }

    public function setIsPast(bool $IsPast): self
    {
        $this->IsPast = $IsPast;

        return $this;
    }

    public function getDuration(): ?\DateInterval
    {
        return $this->Duration;
    }

    public function setDuration(\DateInterval $Duration): self
    {
        $this->Duration = $Duration;

        return $this;
    }
}
