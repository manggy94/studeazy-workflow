<?php

namespace App\Entity;

<<<<<<< HEAD
=======
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
>>>>>>> agenda
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatterRepository")
 */
class Matter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
<<<<<<< HEAD
     * @ORM\Column(type="integer")
     */
    private $DisciplineId;

    /**
     * @ORM\Column(type="integer")
     */
    private $SchoolLevelId;
=======
     * @ORM\ManyToOne(targetEntity="App\Entity\Discipline")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Discipline;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SchoolLevel")
     * @ORM\JoinColumn(nullable=false)
     */
    private $SchoolLevel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersMatter", mappedBy="Matter", orphanRemoval=true)
     */
    private $usersMatters;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lesson", mappedBy="Matter")
     */
    private $lessons;

    public function __construct()
    {
        $this->usersMatters = new ArrayCollection();
        $this->lessons = new ArrayCollection();
    }
>>>>>>> agenda

    public function getId(): ?int
    {
        return $this->id;
    }

<<<<<<< HEAD

    public function getDisciplineId(): ?int
    {
        return $this->DisciplineId;
    }

    public function setDisciplineId(int $DisciplineId): self
    {
        $this->DisciplineId = $DisciplineId;
=======
    public function getDiscipline(): ?Discipline
    {
        return $this->Discipline;
    }

    public function setDiscipline(?Discipline $Discipline): self
    {
        $this->Discipline = $Discipline;

        return $this;
    }

    public function getSchoolLevel(): ?SchoolLevel
    {
        return $this->SchoolLevel;
    }

    public function setSchoolLevel(?SchoolLevel $SchoolLevel): self
    {
        $this->SchoolLevel = $SchoolLevel;

        return $this;
    }

    /**
     * @return Collection|UsersMatter[]
     */
    public function getUsersMatters(): Collection
    {
        return $this->usersMatters;
    }

    public function addUsersMatter(UsersMatter $usersMatter): self
    {
        if (!$this->usersMatters->contains($usersMatter)) {
            $this->usersMatters[] = $usersMatter;
            $usersMatter->setMatter($this);
        }

        return $this;
    }

    public function removeUsersMatter(UsersMatter $usersMatter): self
    {
        if ($this->usersMatters->contains($usersMatter)) {
            $this->usersMatters->removeElement($usersMatter);
            // set the owning side to null (unless already changed)
            if ($usersMatter->getMatter() === $this) {
                $usersMatter->setMatter(null);
            }
        }
>>>>>>> agenda

        return $this;
    }

<<<<<<< HEAD
    public function getSchoolLevelId(): ?int
    {
        return $this->SchoolLevelId;
    }

    public function setSchoolLevelId(int $SchoolLevelId): self
    {
        $this->SchoolLevelId = $SchoolLevelId;
=======
    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->setMatter($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            // set the owning side to null (unless already changed)
            if ($lesson->getMatter() === $this) {
                $lesson->setMatter(null);
            }
        }
>>>>>>> agenda

        return $this;
    }
}
