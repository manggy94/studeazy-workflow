<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $TypeId;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $Start;

    /**
     * @ORM\Column(type="time")
     */
    private $End;

    /**
     * @ORM\Column(type="dateinterval")
     */
    private $Duration;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsPast;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeId(): ?int
    {
        return $this->TypeId;
    }

    public function setTypeId(int $TypeId): self
    {
        $this->TypeId = $TypeId;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->Start;
    }

    public function setStart(\DateTimeInterface $Start): self
    {
        $this->Start = $Start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->End;
    }

    public function setEnd(\DateTimeInterface $End): self
    {
        $this->End = $End;

        return $this;
    }

    public function getDuration(): ?\DateInterval
    {
        return $this->Duration;
    }

    public function setDuration(\DateInterval $Duration): self
    {
        $this->Duration = $Duration;

        return $this;
    }

    public function getIsPast(): ?bool
    {
        return $this->IsPast;
    }

    public function setIsPast(bool $IsPast): self
    {
        $this->IsPast = $IsPast;

        return $this;
    }
}
