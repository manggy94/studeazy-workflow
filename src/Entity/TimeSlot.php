<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use DateInterval;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimeSlotRepository")
 */
class TimeSlot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $Date;

    /**
     * @ORM\Column(type="time")
     */
    private $start;

    /**
     * @ORM\Column(type="time")
     */
    private $end;

    /**
     * @ORM\Column(type="dateinterval")
     */
    private $Duration;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsPast;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getDuration(): ?\DateInterval
    {
        return $this->Duration;
    }

    public function setDuration()
    {
        $start=$this->start;
        $end=$this->end;
        $this->Duration = $start->diff($end);

        return $this;
    }

    public function getIsPast(): ?bool
    {
        return $this->IsPast;
    }

    public function setIsPast()
    {
        $time=new DateTime();
			if ($this->Date > $time->format('Y-m-d')){
				$this->IsPast= false;
			}elseif($this->Date < $time->format('Y-m-d')) {
				$this->IsPast=true;
			}else{
				if ($this->End > $time->format('H:i:s')){
					$this->IsPast=false;
				}else{
					$this->IsPast=true;
				}
			}

        return $this;
    }
    
    //Cette fonction overlap doit prendre deux TimeSlots, et renvoyer leur intersection dans le temps si elle existe, ou false si il n'y a pas d'intersection.
		public function overlap ($TimeSlot2)
        {
            $TimeSlot1 = $this;
			//On definit d'abord l'intersection comme un TimeSlot vide
			$intersection = new TimeSlot();		
			//On definit le 1er TimeSlot comme celui de l'objet en question, le deuxieme etant celui utilise dans la fonction pour le comparer
			//Si les deux TimeSlots sont a la meme Date:
			if ($TimeSlot1->Date == $TimeSlot2->Date){
                dump ('la date est la meme');
				//Si le 1er TimeSlot commence avant le second
				if ($TimeSlot1->start < $TimeSlot2->start){
					dump ('et le 1er TS commence avant le second');
					//Si il finit avant le debut du second
					if ($TimeSlot1->end < $TimeSlot2->start){
						//l'intervalle est vide, on renvoie faux
						return false;
					}
					// sinon, si le 1er finit avant la fin du second, alors
					else if ($TimeSlot1->end < $TimeSlot2->end){
						//on prend  le debut du second comme debut de l'intersection
						$intersection->start = $TimeSlot2->start;
						//On prend la fin du premier comme fin de l'intersection
						$intersection->end = $TimeSlot1->end;
						//et on ajoute la Date
						$intersection->Date = $TimeSlot1->Date;
						//On met l'id du 2eme TimeSlot dans l'id_TimeSlot de l'intervalle:
						$intersection->setDuration();
						$intersection->setIsPast();
						
						//On renvoie l'intersection ainsi cree
						
						return $intersection;
					}
					//Sinon, le deuxieme TimeSlot est inclus dans le premier
					else{
						//l'intersection est donc le deuxieme TimeSlot, que l'on renvoie
						$intersection->start = $TimeSlot2->start;
						//On prend la fin du 2eme comme fin de l'intersection
						$intersection->end = $TimeSlot2->end;
						//et on ajoute la Date
						$intersection->Date = $TimeSlot2->Date;
						//On renvoie l'intersection ainsi cree
						$intersection->setDuration();
						$intersection->setIsPast();
							
						return $intersection;
					}
				}
				//Sinon, le deuxieme TimeSlot commence avant le premier, ou en meme temps
				else{
                    dump ('et le 1er TS commence apres le second ou en meme temps');
					//dans ce cas, si le 2eme TimeSlot finit avant le debut du premier,
					if ($TimeSlot2->end < $TimeSlot1->start){
						//l'intervalle est vide, on renvoie faux
						return false;
					}
					// sinon, si le 2eme finit avant la fin du 1er, alors
					else if ($TimeSlot2->end < $TimeSlot1->end){
						
						//On prend le debut du 1er comme debut de l'intersection
						$intersection->start = $TimeSlot1->start;
						//On prend la fin du 2eme comme fin de l'intersection
						$intersection->end = $TimeSlot2->end;
						//et on ajoute la Date
						$intersection->Date = $TimeSlot1->Date;
						//On renvoie l'intersection ainsi cree
						$intersection->setDuration();
						$intersection->setIsPast();
						return $intersection;
					}
					//Sinon, le 1er TimeSlot est inclus dans le 2nd
					else{
					//	echo 'still no problem <br>';
						//l'intersection est donc le 1er TimeSlot, que l'on renvoie
						//$intersection->get_TimeSlot($TimeSlot1->get_id_TimeSlot());
						$intersection->start = $TimeSlot1->start;
						//On prend la fin du 2eme comme fin de l'intersection
						$intersection->end = $TimeSlot1->end;
						//et on ajoute la Date
						$intersection->Date = $TimeSlot1->Date;
						//On renvoie l'intersection ainsi cree
						$intersection->setDuration();
						$intersection->setIsPast();
						return $intersection;
					}
				}
			}else{
				return false;
			}
		}
}
