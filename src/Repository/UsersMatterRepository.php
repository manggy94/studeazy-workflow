<?php

namespace App\Repository;

use App\Entity\UsersMatter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsersMatter|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersMatter|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersMatter[]    findAll()
 * @method UsersMatter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersMatterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersMatter::class);
    }

    // /**
    //  * @return UsersMatter[] Returns an array of UsersMatter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersMatter
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
