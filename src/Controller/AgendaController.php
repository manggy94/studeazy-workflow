<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Availability;
use App\Entity\TimeSlot;
use App\Entity\User;
use App\Repository\AvailabilityRepository;
use App\Repository\TimeSlotRepository;
use DateTime;
use App\Form\TimeSlotType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AgendaController extends AbstractController
{
    /**
     *@var AvailabilityRepository
     */
    private $repository;
    
    public function __construct(AvailabilityRepository $AvailabilityRepository, TimeSlotRepository $TimeSlotRepository)
    {
        $this->AvRepository=$AvailabilityRepository;
        $this->TSRepository=$TimeSlotRepository;
    }
    
    /**
     * @Route("/agenda", name="agenda")
     */
    public function index(Request $request,ValidatorInterface $validator)
    {
        $user = $this->getUser();
        $availability=new Availability();
        $TimeSlot=new TimeSlot();
        $availabilities=$this->AvRepository->findByUser($user);
        $TimeSlots=$this->TSRepository->findAll();
        dump($TimeSlots);
        
        
        $form=$this->createForm(TimeSlotType::class, $TimeSlot);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){ 
            //TimeSlotType data is inserted into $TimeSlot entity
            $Date=new DateTime($form->get('Date')->getData());
            $start=new DateTime('1970-01-01 '.$form->get('start')->getData());
            $end = new DateTime('1970-01-01 '.$form->get('end')->getData());
            $TimeSlot->setDate($Date);
            $TimeSlot->setStart($start);
            $TimeSlot->setEnd($end);
            $TimeSlot->setDuration();
            $TimeSlot->setIsPast();
            
            $availability->setTimeSlot($TimeSlot);
            $availability->setUser($user);
            $condition = true;
            dump ($condition);   
            if ($condition==true)
            {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($availability,$TimeSlot);
                $entityManager->flush(); 
            }
            else
            {
                dump ('Les dispos se chevauchent');
            }
            
           
        }
        $availabilities=$this->AvRepository->findByUser($user);
        $TimeSlots=$this->TSRepository->findAll();
       
        $lessons=$user->getLessons();
        $events = array();
        foreach ($availabilities as $av)
        {
            $event=array("id"=>$av->getTimeSlot()->getId(),'start'=>$av->getTimeSlot()->getDate()->format('Y-m-d').'T'.$av->getTimeSlot()->getStart()->format('H:i:s'),'end'=>$av->getTimeSlot()->getDate()->format('Y-m-d').'T'.$av->getTimeSlot()->getEnd()->format('H:i:s'),'title'=>'availability')
                         ;
            array_push($events, $event);            
        }
        //$events=json_encode($events);
        dump($availabilities);
        dump($user);
        //dump($lessons);
        //dump($TimeSlot);
        //dump($availability);
        dump($events);
        return $this->render('agenda/index.html.twig', [
            'controller_name' => 'AgendaController',
            'user' => $user,
            'lessons'=> $lessons,
            'availabilities' => $availabilities,
            'form'=>$form->createView(),
            'events'=>$events,
        ]);
    }
}
