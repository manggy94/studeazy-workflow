<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Address;
use DateTime;
use App\Service\Gmap\GmapApi;
use App\Form\CreateUserType;
use App\Form\RegistrationFormType;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $_POST['fisrtAuthentication'] = true;
            //dump($_POST);


            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/register/create", name="create_profile")
     */
    public function createProfile(Request $request, GmapApi $api, ValidatorInterface $validator)
	{
		$user= $this->getUser();
        $address=new Address();
        
		$form=$this->createForm(CreateUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){ 
            //CreateUserType data is inserted into $user entity
            $user->setCategory($form->get('Category')->getData());
            $user->setFirstName($form->get('firstName')->getData());
            $user->setLastName($form->get('lastName')->getData());
            $user->setSurname($form->get('surname')->getData());
            $birthday=new DateTime($form->get('birthday')->getData());
            $user->setBirthday($birthday);
            //Address is defined by using GmapApi
            $gmapAddress=$api->geocodeAddress($form->get('Address')->getdata())['results'][0];
            $address->setGmapId($gmapAddress['place_id']);
            $address->setUser($user);
            $address->setLabel($gmapAddress['formatted_address']);
            $address->setLat($gmapAddress['geometry']['location']['lat']);
            $address->setLng($gmapAddress['geometry']['location']['lng']);
            $address->setStreetNumber($gmapAddress['address_components'][0]['long_name']);
            $address->setRoute($gmapAddress['address_components'][1]['long_name']);
            $address->setLocality($gmapAddress['address_components'][2]['long_name']);
            $address->setAdministrativeAreaLevel2($gmapAddress['address_components'][3]['long_name']);
            $address->setAdministrativeAreaLevel1($gmapAddress['address_components'][4]['long_name']);
            $address->setCountry($gmapAddress['address_components'][5]['long_name']);
            $address->setPostalCode($gmapAddress['address_components'][6]['long_name']);
            $user->setAddress($address);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user,$address);
            $entityManager->flush();
            return new RedirectResponse($this->generateUrl('profile'));

        //    $user->setProfilePic('xx');
        //    $user->setAddress($api->geocodeAddress($form->get('Address')->getdata()));
        //    $user->setAvatar('zzzz');
        //    
        }
        //dump($user);
        //dump($address);
		return $this->render('registration/create_profile.html.twig', [
            'controller_name' => 'RegistrationController',
            'user'=>$user,
            'form'=>$form->createView()
        ]);
	}
}
