<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Response;

class LandingController extends AbstractController
{
	
	/**
	 *@Route("/landing/parents", name="landing_parents")
	 */
	public function index(): Response
	{
		$parent='Sidonie';
		return $this->render('pages/landing/parents.html.twig');
	}
	
}