<?php

namespace App\Form;

use App\Entity\TimeSlot;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeSlotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Date', TextType::class, [
                'mapped' => false,
                'label' => 'Date',
                'attr' => [
                    'placeholder' => 'Cliquez pour ajouter une date'
                ]
            ])
            ->add('start', TextType::class, [
                'mapped' => false,
                'label' => 'Heure de début',
                'attr' => [
                    'placeholder' => 'Cliquez pour ajouter heure de début'
                ]
            ])
            ->add('end', TextType::class, [
                'mapped' => false,
                'label' => 'Heure de fin',
                'attr' => [
                    'placeholder' => 'Cliquez pour ajouter une heure de fin'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TimeSlot::class,
        ]);
    }
}
