<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserCategory;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => 'Votre prénom'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Votre nom'
                ]
            ])
            ->add('surname', TextType::class, [
                'label' => 'Surnom',
                'attr' => [
                    'placeholder' => 'Choisissez un surnom'
                ]
            ])
            ->add('birthday', TextType::class, [
                'mapped' => false,
                'label' => 'Date de naissance',
                'attr' => [
                    'placeholder' => 'Cliquez pour ajouter votre date de naissance'
                ]
            ])
            
            ->add('Category', EntityType::class,
                  [
                    'class'=>UserCategory::class,
                    'choice_label'=>'Category',
                    'label'=>"Vous êtes...",
                    'expanded'=>true,
                    'multiple'=>false,
                   ])
            
            ->add('Address', TextType::class,
                  [
                    'mapped'=>false,
                    'attr'=>[
                        'placeholder'=>'Entrez votre adresse'
                    ],
                    'label'=>"Adresse",
                   ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
