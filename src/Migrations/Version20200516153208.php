<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516153208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE availability ADD time_slot_id INT DEFAULT NULL, DROP date, DROP start, DROP end, DROP duration, DROP is_past, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE availability ADD CONSTRAINT FK_3FB7A2BFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE availability ADD CONSTRAINT FK_3FB7A2BFD62B0FA FOREIGN KEY (time_slot_id) REFERENCES time_slot (id)');
        $this->addSql('CREATE INDEX IDX_3FB7A2BFA76ED395 ON availability (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3FB7A2BFD62B0FA ON availability (time_slot_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE availability DROP FOREIGN KEY FK_3FB7A2BFA76ED395');
        $this->addSql('ALTER TABLE availability DROP FOREIGN KEY FK_3FB7A2BFD62B0FA');
        $this->addSql('DROP INDEX IDX_3FB7A2BFA76ED395 ON availability');
        $this->addSql('DROP INDEX UNIQ_3FB7A2BFD62B0FA ON availability');
        $this->addSql('ALTER TABLE availability ADD date DATE NOT NULL, ADD start TIME NOT NULL, ADD end TIME NOT NULL, ADD duration VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:dateinterval)\', ADD is_past TINYINT(1) NOT NULL, DROP time_slot_id, CHANGE user_id user_id INT NOT NULL');
    }
}
