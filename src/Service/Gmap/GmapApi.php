<?php

namespace App\Service\Gmap;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GmapApi
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function geocodeAddress(string $Address)
    {
		$apikey = 'AIzaSyCDonhoRu6kqtYR0kEnRb2_P7c4yWR5GJo';
      $url="https://maps.google.com/maps/api/geocode/json?key=".$apikey."&address=$Address&sensor=false&region=fr";
      //$url = 'https://api-adresse.data.gouv.fr/search/?q='.$Address.'&autocomplete=1&limit=2';
        
        $response = $this->httpClient->request('GET', $url, [
                'headers' => [
                    'accept' => 'application/json'
                ]
            ]
        );

        return $response->toArray();
    }
	 
	  public function autocomplete(string $Address)
    {
		$apikey = 'AIzaSyCDonhoRu6kqtYR0kEnRb2_P7c4yWR5GJo';
      $url="https://maps.google.com/maps/api/place/autocomplete/json?input=.$Address.&key=".$apikey."&types=geocode&language=fr&components=country:fr";
      //$url = 'https://api-adresse.data.gouv.fr/search/?q='.$Address.'&autocomplete=1&limit=2';
        
        $response = $this->httpClient->request('GET', $url, [
                'headers' => [
                    'accept' => 'application/json'
                ]
            ]
        );

        return $response->toArray();
    }
}