<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class AddressApi
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function searchAddress(string $dataAddress)
    {
        //$url = 'https://api-adresse.data.gouv.fr/search/?q='.$dataAddress.'&type=municipality&autocomplete=1';
        $url = 'https://api-adresse.data.gouv.fr/search/?q='.$dataAddress.'&autocomplete=1&limit=2';
        
        $response = $this->httpClient->request('GET', $url, [
                'headers' => [
                    'accept' => 'application/json'
                ]
            ]
        );

        return $response->toArray();
    }
}